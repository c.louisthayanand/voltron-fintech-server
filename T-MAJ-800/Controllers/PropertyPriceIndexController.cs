﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Models;
using T_MAJ_800.Repositories;

namespace T_MAJ_800.Controllers
{
    /// <summary>
    /// HPM09
    /// </summary>
    [ApiController]
    [Route("propertyPriceIndex")]
    public class PropertyPriceIndexController : ControllerBase
    {

        private readonly ILogger<PropertyPriceIndexController> _logger;

        public PropertyPriceIndexController(ILogger<PropertyPriceIndexController> logger)
        {
            _logger = logger;
        }

        [HttpGet("possibleFilters")]
        public ActionResult<List<apiPossibleFilter>> GetPossibleFilters()
        {
            return new ObjectResult(DatasetRepository.GetPossibleFilters(eDataset.HPM09))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }

        /// <summary>
        /// HPM09.  Monthly data.
        /// </summary>
        /// <remarks>
        /// HPM09 - Residential Property Price Index by Type of Residential Property, Month and Statistic
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<apiFilteredDataset> GetDwellingSalesVolume([FromQuery(Name = "q")] string query, [FromQuery] int epPoints = 5, [FromQuery] ExtrapolationMethods epMethod = ExtrapolationMethods.Linear)
        {
            return new ObjectResult(DatasetRepository.GetFilteredDataset(eDataset.HPM09, query, epPoints, epMethod))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }
    }
}
