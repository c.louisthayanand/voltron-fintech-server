﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Models;
using T_MAJ_800.Repositories;

namespace T_MAJ_800.Controllers
{
    /// <summary>
    /// HPM03
    /// </summary>
    [ApiController]
    [Route("dwellingSalesVolume")]
    public class DwellingSalesVolumeController : ControllerBase
    {

        private readonly ILogger<DwellingSalesVolumeController> _logger;

        public DwellingSalesVolumeController(ILogger<DwellingSalesVolumeController> logger)
        {
            _logger = logger;
        }

        [HttpGet("possibleFilters")]
        public ActionResult<List<apiPossibleFilter>> GetPossibleFilters()
        {
            return new ObjectResult(DatasetRepository.GetPossibleFilters(eDataset.HPM03))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }

        /// <summary>
        /// HPM03.  Montlhy data.
        /// </summary>
        /// <remarks>
        /// HPM03 - Market-based Household Purchases of Residential Dwellings by Dwelling Status, Stamp Duty Event, RPPI Region, Type of Buyer, Month and Statistic
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<apiFilteredDataset> GetDwellingSalesVolume([FromQuery(Name = "q")] string query, [FromQuery] int epPoints = 5, [FromQuery] ExtrapolationMethods epMethod = ExtrapolationMethods.Linear)
        {
            return new ObjectResult(DatasetRepository.GetFilteredDataset(eDataset.HPM03, query, epPoints, epMethod))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }
    }
}
