﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Models;
using T_MAJ_800.Repositories;

namespace T_MAJ_800.Controllers
{
    [ApiController]
    [Route("rolling12Months")]
    public class Rolling12MonthController : ControllerBase
    {
        private readonly ILogger<Rolling12MonthController> _logger;

        public Rolling12MonthController(ILogger<Rolling12MonthController> logger)
        {
            _logger = logger;
        }

        [HttpGet("possibleFilters")]
        public ActionResult<List<apiPossibleFilter>> GetPossibleFilters()
        {
            return new ObjectResult(DatasetRepository.GetPossibleFilters(eDataset.HPM07))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }

        /// <summary>
        /// HPM07.  Monthly data.
        /// </summary>
        /// <remarks>
        /// HPM07 - Rolling 12 Month Market-based Household Purchases of Residential Dwellings by Dwelling Status, Stamp Duty Event, RPPI Region, Type of Buyer, Month and Statistic
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<apiFilteredDataset> GetDwellingSalesVolume([FromQuery(Name = "q")] string query, [FromQuery] int epPoints = 5, [FromQuery] ExtrapolationMethods epMethod = ExtrapolationMethods.Linear)
        {
            return new ObjectResult(DatasetRepository.GetFilteredDataset(eDataset.HPM07, query, epPoints, epMethod))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }
    }
}
