﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Models;
using T_MAJ_800.Repositories;

namespace T_MAJ_800.Controllers
{
    /// <summary>
    /// NDA02
    /// </summary>
    [ApiController]
    [Route("newDwellingsCompletion")]
    public class NewDwellingsCompletionController : ControllerBase
    {
        private readonly ILogger<NewDwellingsCompletionController> _logger;

        public NewDwellingsCompletionController(ILogger<NewDwellingsCompletionController> logger)
        {
            _logger = logger;
        }

        [HttpGet("possibleFilters")]
        public ActionResult<List<apiPossibleFilter>> GetPossibleFilters()
        {
            return new ObjectResult(DatasetRepository.GetPossibleFilters(eDataset.NDA02))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }

        /// <summary>
        /// NDA02.  Yearly data.
        /// </summary>
        /// <remarks>
        /// NDA02 - NDA02 - New Dwelling Completions (Number) by Type of House and Year
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<apiFilteredDataset> GetNewDwellings([FromQuery(Name = "q")] string query, [FromQuery] int epPoints = 5, [FromQuery] ExtrapolationMethods epMethod = ExtrapolationMethods.Linear)
        {
            return new ObjectResult(DatasetRepository.GetFilteredDataset(eDataset.NDA02, query, epPoints, epMethod))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }
    }
}
