﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Models;
using T_MAJ_800.Repositories;

namespace T_MAJ_800.Controllers
{
    /// <summary>
    /// HPM02
    /// </summary>
    [ApiController]
    [Route("residentialTransactions")]
    public class ResidentialTransactionsController : ControllerBase
    {
        private readonly ILogger<ResidentialTransactionsController> _logger;

        public ResidentialTransactionsController(ILogger<ResidentialTransactionsController> logger)
        {
            _logger = logger;
        }

        [HttpGet("possibleFilters")]
        public ActionResult<List<apiPossibleFilter>> GetPossibleFilters()
        {
            return new ObjectResult(DatasetRepository.GetPossibleFilters(eDataset.HPM02))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }

        /// <summary>
        /// HPM02.  Monthly data.
        /// </summary>
        /// <remarks>
        /// HPM02 - Residential Dwelling Property Transactions by County, Dwelling Status, Stamp Duty Event, Type of Buyer, Type of Sale, Month and Statistic
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<apiFilteredDataset> GetDwellingSalesVolume([FromQuery(Name = "q")] string query, [FromQuery] int epPoints = 5, [FromQuery] ExtrapolationMethods epMethod = ExtrapolationMethods.Linear)
        {
            return new ObjectResult(DatasetRepository.GetFilteredDataset(eDataset.HPM02, query, epPoints, epMethod))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }
    }
}
