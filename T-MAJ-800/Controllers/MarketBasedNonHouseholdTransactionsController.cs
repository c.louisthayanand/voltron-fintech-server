﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Models;
using T_MAJ_800.Repositories;

namespace T_MAJ_800.Controllers
{
    /// <summary>
    /// HPA10
    /// </summary>
    [ApiController]
    [Route("marketBasedNonHouseholdTransactions")]
    public class MarketBasedNonHouseholdTransactionsController : ControllerBase
    {
        private readonly ILogger<MarketBasedNonHouseholdTransactionsController> _logger;

        public MarketBasedNonHouseholdTransactionsController(ILogger<MarketBasedNonHouseholdTransactionsController> logger)
        {
            _logger = logger;
        }

        [HttpGet("possibleFilters")]
        public ActionResult<List<apiPossibleFilter>> GetPossibleFilters()
        {
            return new ObjectResult(DatasetRepository.GetPossibleFilters(eDataset.HPA10))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }

        /// <summary>
        /// HPA10.  Yearly data.
        /// </summary>
        /// <remarks>
        /// HPA10 - Market-based Non-Household Transactions of Residential Dwellings by Dwelling Status, Participant, Stamp Duty Event, NACE Section, Year and Statistic
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<apiFilteredDataset> GetDwellingSalesVolume([FromQuery(Name = "q")] string query, [FromQuery] int epPoints = 5, [FromQuery] ExtrapolationMethods epMethod = ExtrapolationMethods.Linear)
        {
            return new ObjectResult(DatasetRepository.GetFilteredDataset(eDataset.HPA10, query, epPoints, epMethod))
            {
                StatusCode = StatusCodes.Status200OK
            };
        }
    }
}
