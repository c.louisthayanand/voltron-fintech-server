﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Models;
using T_MAJ_800.Repositories;

namespace T_MAJ_800.Controllers
{
    /// <summary>
    /// HPM03
    /// </summary>
    [ApiController]
    [Route("datasets/update")]
    public class DatasetsController : ControllerBase
    {

        private readonly ILogger<DatasetsController> _logger;

        public DatasetsController(ILogger<DatasetsController> logger)
        {
            _logger = logger;
        }


        /// <summary>
        /// Update all datasets in blob storage.
        /// </summary>
        /// <remarks></remarks>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<apiPossibleFilter>> UpdateAllDatasets()
        {
            var datasets = Enum.GetValues(typeof(eDataset)).Cast<eDataset>();
            var _blobRepository = new BlobRepository();

            Parallel.ForEach(datasets, dataset => {
                _blobRepository.UpdateDatasetBlobContent(dataset);
            });

            return new ObjectResult("Update done.")
            {
                StatusCode = StatusCodes.Status200OK
            };
        }
    }
}
