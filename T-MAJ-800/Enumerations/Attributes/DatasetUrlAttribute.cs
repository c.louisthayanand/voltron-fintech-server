﻿using System;

namespace T_MAJ_800.Enumerations.Attributes
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
    public class DatasetUrlAttribute : Attribute
    {
        public DatasetUrlAttribute(string url, string apiKey = null)
        {
            Url = url;
            ApiKey = apiKey;
        }

        public string Url { get; }
        public string ApiKey { get; }
    }
}
