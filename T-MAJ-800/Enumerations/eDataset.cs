﻿using T_MAJ_800.Enumerations.Attributes;
using T_MAJ_800.Helpers;

namespace T_MAJ_800.Enumerations
{
    public enum eDataset
    {
        /// <summary>
        /// HPM02 - Residential Dwelling Property Transactions by County, Dwelling Status, Stamp Duty Event, Type of Buyer, Type of Sale, Month and Statistic
        /// </summary>
        [DatasetUrl(ConfigHelper.BASE_URL_STATEBANK_IRELAND + "HPM02")]
        HPM02,

        /// <summary>
        /// HPM03 - Market-based Household Purchases of Residential Dwellings by Dwelling Status, Stamp Duty Event, RPPI Region, Type of Buyer, Month and Statistic
        /// </summary>
        [DatasetUrl(ConfigHelper.BASE_URL_STATEBANK_IRELAND + "HPM03")]
        HPM03,

        /// <summary>
        /// HPM09 - Residential Property Price Index by Type of Residential Property, Month and Statistic
        /// </summary>
        [DatasetUrl(ConfigHelper.BASE_URL_STATEBANK_IRELAND + "HPM09")]
        HPM09,

        /// <summary>
        /// HPA10 - Market-based Non-Household Transactions of Residential Dwellings by Dwelling Status, Participant, Stamp Duty Event, NACE Section, Year and Statistic
        /// </summary>
        [DatasetUrl(ConfigHelper.BASE_URL_STATEBANK_IRELAND + "HPA10")]
        HPA10,

        /// <summary>
        /// HPM07 - Rolling 12 Month Market-based Household Purchases of Residential Dwellings by Dwelling Status, Stamp Duty Event, RPPI Region, Type of Buyer, Month and Statistic
        /// </summary>
        [DatasetUrl(ConfigHelper.BASE_URL_STATEBANK_IRELAND + "HPM07")]
        HPM07,

        /// <summary>
        /// NDA02 - New Dwelling Completions (Number) by Type of House and Year
        /// </summary>
        [DatasetUrl(ConfigHelper.BASE_URL_STATEBANK_IRELAND + "NDA02")]
        NDA02
    }
}
