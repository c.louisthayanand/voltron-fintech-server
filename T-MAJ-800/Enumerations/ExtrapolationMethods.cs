﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace T_MAJ_800.Enumerations
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ExtrapolationMethods
    {
        Linear,
        Common,
        CubicSpline,
        CubicSplineRobust,
        PolynomialEquidistant,
        RationalWithoutPoles,
        RationalWithPoles,
        Step
    }
}
