﻿using System.Collections.Generic;

namespace T_MAJ_800.Models
{

    public class apiPossibleFilter
    {
        public string Name { get; set; }
        public Dictionary<string, int> Values { get; set; }

        public apiPossibleFilter(string name, Dictionary<string, int> values)
        {
            Name = name;
            Values = values;
        }
    }
}
