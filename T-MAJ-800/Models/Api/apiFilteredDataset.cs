﻿using System.Collections.Generic;

namespace T_MAJ_800.Models
{
    public class apiFilteredDataset
    {
        public Dictionary<string, string> FiltersApplied { get; set; }
        public List<apiMetric> Metrics { get; set; }
        public List<apiData> Data { get; set; }

        public apiFilteredDataset(Dictionary<string, string> filtersApplied, List<apiMetric> metrics, List<apiData> data)
        {
            FiltersApplied = filtersApplied;
            Metrics = metrics;
            Data = data;
        }
    }
}
