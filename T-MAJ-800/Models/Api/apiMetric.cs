﻿namespace T_MAJ_800.Models
{
    public class apiMetric
    {
        public string Name { get; set; }
        public int Index { get; set; }
        public string Unit { get; set; }

        public apiMetric(string name, int index, string unit)
        {
            Name = name;
            Index = index;
            Unit = unit;
        }
    }
}
