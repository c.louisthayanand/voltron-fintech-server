﻿using System.Collections.Generic;

namespace T_MAJ_800.Models
{
    public class apiData
    {
        public string X { get; set; }
        public List<float?> Y { get; set; }

        public apiData(string x, List<float?> y)
        {
            X = x;
            Y = y;
        }
    }
}
