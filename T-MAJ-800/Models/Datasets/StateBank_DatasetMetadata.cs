﻿using System;
using System.Collections.Generic;

namespace T_MAJ_800.Models
{
    public class StateBank_Dataset
    {
        public StateBank_DatasetObject Dataset { get; set; }
    }

    public class StateBank_DatasetObject
    {
        // StateBank_Dimension_Meta with Dictionary<string, StateBank_Dimension>
        public object Dimension { get; set; }
        public string Label { get; set; }
        public string Source { get; set; }
        public DateTimeOffset Updated { get; set; }
        public List<float?> Value { get; set; }
    }
}
