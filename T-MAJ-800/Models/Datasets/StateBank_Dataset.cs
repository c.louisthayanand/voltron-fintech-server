﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace T_MAJ_800.Models
{
    public class StateBank_DatasetMetadata
    {
        public List<apiPossibleFilter> Filters;
        public List<apiMetric> Metrics;
        public List<string> Abscissa;

        public StateBank_DatasetMetadata(StateBank_DatasetObject dataset)
        {
            try
            {
                var dimensions = JObject.FromObject(dataset.Dimension);

                var ids = dimensions.Values<string>("id");
                var sizes = dimensions.Values<int>("size");
                var roles = dimensions["role"];
                var times = ((JArray)roles["time"]).ToObject<List<string>>();
                var metrics = ((JArray)roles["metric"]).ToObject<List<string>>();

                var propertiesToFilter = new List<string>() { "id", "size", "role" };
                propertiesToFilter.AddRange(times);
                propertiesToFilter.AddRange(metrics);
                var keys = dimensions.Properties().Select(p => p.Name).Where(p => !propertiesToFilter.Contains(p)).ToList();

                #region Filters

                // Get filters

                var filters = new List<apiPossibleFilter>();
                foreach (var key in keys)
                {
                    var category = dimensions[key];

                    // Get data indexes
                    var valuesAndIndexes = (JObject)category["category"]["index"];
                    var valuesAndIndexes_Dictionary = valuesAndIndexes.ToObject<Dictionary<string, int>>();

                    // Get labels
                    var valuesAndLabels = (JObject)category["category"]["label"];
                    var valuesAndLabels_Dictionary = valuesAndLabels.ToObject<Dictionary<string, string>>();

                    // Match labels to data indexes
                    var labelsAndIndexes = new Dictionary<string, int>();
                    foreach (var property_key in valuesAndIndexes_Dictionary.Keys)
                    {
                        var label = valuesAndLabels_Dictionary.GetValueOrDefault(property_key);
                        var index = valuesAndIndexes_Dictionary.GetValueOrDefault(property_key);
                        labelsAndIndexes.Add(label, index);
                    }


                    var filter = new apiPossibleFilter(key, labelsAndIndexes);
                    filters.Add(filter);
                }

                Filters = filters;

                #endregion


                #region Metrics

                // Get metrics
                var apiMetrics = new List<apiMetric>();
                foreach (var metric in metrics)
                {
                    var metric_declaration = dimensions[metric];

                    // Get data indexes
                    var valuesAndIndexes = (JObject)metric_declaration["category"]["index"];
                    var valuesAndIndexes_Dictionary = valuesAndIndexes.ToObject<Dictionary<string, int>>();

                    // Get labels
                    var valuesAndLabels = (JObject)metric_declaration["category"]["label"];
                    var valuesAndLabels_Dictionary = valuesAndLabels.ToObject<Dictionary<string, string>>();

                    // Get units
                    var valuesAndUnit = (JObject)metric_declaration["category"]["unit"];
                    var valuesAndUnit_Dictionary = valuesAndUnit.ToObject<Dictionary<string, KeyValuePair<string, string>>>();

                    // Match labels to data indexes and units
                    var labelsIndexesAndUnits = new Dictionary<string, KeyValuePair<string, int>>();
                    foreach (var property_key in valuesAndIndexes_Dictionary.Keys)
                    {
                        var label = valuesAndLabels_Dictionary.GetValueOrDefault(property_key);
                        var index = valuesAndIndexes_Dictionary.GetValueOrDefault(property_key);
                        var unit = valuesAndUnit_Dictionary.GetValueOrDefault(property_key);
                        apiMetrics.Add(new apiMetric(label, index, unit.Value));
                    }
                }

                Metrics = apiMetrics;

                #endregion


                #region Abscissa

                // Get labels
                var abscissa = (JObject)dimensions[times[0]]["category"]["label"];
                var abscissa_Dictionary = abscissa.ToObject<Dictionary<string, string>>();
                Abscissa = abscissa_Dictionary.Select(x => x.Value).ToList();

                #endregion

            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong");
            }
        }
    }
}
