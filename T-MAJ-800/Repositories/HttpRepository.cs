﻿using System.IO;
using System.Net;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Helpers.Enumeration;

namespace T_MAJ_800.Repositories
{
    public class HttpRepository
    {
        public static string GetDataset(eDataset dataset)
        {
            return Get(dataset.GetDatasetUrl(), dataset.GetDatasetApiKey());
        }

        public static string Get(string uri, string apiKey = null)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "GET";

            return MakeCall(request, apiKey);
        }

        private static string MakeCall(HttpWebRequest request, string apiKey = null)
        {
            if (!string.IsNullOrWhiteSpace(apiKey))
                request.Headers.Add(HttpRequestHeader.Authorization, apiKey);

            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using Stream stream = response.GetResponseStream();
            using StreamReader reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }
    }
}
