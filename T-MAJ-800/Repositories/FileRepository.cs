﻿using System;
using System.IO;
using T_MAJ_800.Enumerations;

namespace T_MAJ_800.Repositories
{
    public static class FileRepository
    {
        public static string fileFolder = Environment.GetEnvironmentVariable("DATASETS_FOLDER") ?? @".\Datasets\";

        public static string GetDataset(eDataset dataset)
        {
            return File.ReadAllText($@"{fileFolder}{dataset}.json");
        }
    }
}
