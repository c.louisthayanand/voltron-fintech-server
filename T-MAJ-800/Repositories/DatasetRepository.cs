﻿using MathNet.Numerics;
using MathNet.Numerics.Interpolation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Helpers;
using T_MAJ_800.Models;

namespace T_MAJ_800.Repositories
{
    public class DatasetRepository
    {
        public static StateBank_Dataset GetSourceDataset(eDataset dataset)
        {
            var _blobRepository = new BlobRepository();
            return JsonConvert.DeserializeObject<StateBank_Dataset>(_blobRepository.GetDatasetFromBlob(dataset));
        }

        public static List<apiPossibleFilter> GetPossibleFilters(eDataset dataset)
        {
            var dataset_Object = GetSourceDataset(dataset);
            return new StateBank_DatasetMetadata(dataset_Object.Dataset).Filters;
        }

        public static apiFilteredDataset GetFilteredDataset(eDataset dataset, string query = "", int epPoints = 5, ExtrapolationMethods epMethod = ExtrapolationMethods.Linear)
        {
            if (string.IsNullOrWhiteSpace(query))
                query = "";

            // Get data
            var sourceDataset = GetSourceDataset(dataset);

            // Get possible filters and extract filters from query (ex: ?q=DwellingStatus:1;RPPIRegion:17)
            var datasetMetadata = new StateBank_DatasetMetadata(sourceDataset.Dataset);
            var queryFilters = query.Split(';').Where(q => q.Contains(':')).ToList();
            var filters = new Dictionary<string, KeyValuePair<string, int>>();
            datasetMetadata.Filters.ForEach(f => {
                var filterNameQuery = f.Name.Replace(" ", "").ToLower();
                var filterQuery = queryFilters.FirstOrDefault(q => q.ToLower().StartsWith(filterNameQuery));
                if (!string.IsNullOrEmpty(filterQuery))
                {
                    var index = int.Parse(filterQuery.Split(":")[1]);
                    filters.Add(f.Name, new KeyValuePair<string, int>(f.Values.FirstOrDefault(x => x.Value == index).Key, index));
                }
                else
                {
                    filters.Add(f.Name, f.Values.First());
                }
            });

            // Get indexes
            var dataPerFilter = datasetMetadata.Metrics.Count * datasetMetadata.Abscissa.Count;
            var index_firstValue = 0;
            for (int f = 0; f < filters.Count; f++)
            {
                var filter = filters.ElementAt(filters.Count - (f + 1));
                index_firstValue += filter.Value.Value * dataPerFilter;
                dataPerFilter *= datasetMetadata.Filters.First(f => f.Name == filter.Key).Values.Count;
            }

            var data_List = new List<apiData>();
            var currentIndex = index_firstValue;
            foreach (var abscissa in datasetMetadata.Abscissa)
            {
                var metrics = new List<float?>();
                foreach (var metric in datasetMetadata.Metrics)
                {
                    metrics.Add(sourceDataset.Dataset.Value[currentIndex++]);
                }

                var data = new apiData(abscissa, metrics);
                data_List.Add(data);
            }

            try
            {
                // Interpolate with existing data
                string lastAbscissa = data_List.Last().X;
                int ipPoints = datasetMetadata.Abscissa.Count > 24 ? datasetMetadata.Abscissa.Count - 12 : datasetMetadata.Abscissa.Count;
                List<IInterpolation> interpolations = new List<IInterpolation>();
                List<double> ipAbscissa = Enumerable.Range(1, ipPoints).Select(i => Convert.ToDouble(i)).ToList();
                List<apiData> ipData = data_List.Skip(Math.Max(0, data_List.Count() - ipPoints)).ToList();
                for (int i = 0; i < datasetMetadata.Metrics.Count; i++)
                {
                    List<double> ipMetrics = ipData.Select(d => Convert.ToDouble(d.Y[i] ?? 0)).ToList();

                    interpolations.Add(
                        epMethod switch
                        {
                            ExtrapolationMethods.Linear => Interpolate.Linear(ipAbscissa, ipMetrics),
                            ExtrapolationMethods.Common => Interpolate.Common(ipAbscissa, ipMetrics),
                            ExtrapolationMethods.CubicSpline => Interpolate.CubicSpline(ipAbscissa, ipMetrics),
                            ExtrapolationMethods.CubicSplineRobust => Interpolate.CubicSplineRobust(ipAbscissa, ipMetrics),
                            ExtrapolationMethods.PolynomialEquidistant => Interpolate.PolynomialEquidistant(ipAbscissa, ipMetrics),
                            ExtrapolationMethods.RationalWithoutPoles => Interpolate.RationalWithoutPoles(ipAbscissa, ipMetrics),
                            ExtrapolationMethods.RationalWithPoles => Interpolate.RationalWithPoles(ipAbscissa, ipMetrics),
                            ExtrapolationMethods.Step => Interpolate.Step(ipAbscissa, ipMetrics),
                            _ => Interpolate.Linear(ipAbscissa, ipMetrics)
                        }
                    );
                }

                // Generate extrapolation points
                if (ipPoints > 0 && epPoints > 0)
                {
                    for (int i = 1; i <= epPoints; i++)
                    {
                        double epAbscissa = ipPoints + i;
                        var abscissa = TimeMetricsGenerator.CreateAbscissa(lastAbscissa, i);
                        var metrics = new List<float?>();

                        foreach (var interpolation in interpolations)
                        {
                            metrics.Add(Convert.ToSingle(interpolation.Interpolate(epAbscissa)));
                        }

                        data_List.Add(new apiData(abscissa, metrics));
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Extrapolation failed: {ex.Message}");
            }
            

            return new apiFilteredDataset(filters.ToDictionary(t => t.Key, t => t.Value.Key), datasetMetadata.Metrics, data_List);
        }
    }
}
