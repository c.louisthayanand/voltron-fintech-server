﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using T_MAJ_800.Enumerations;
using T_MAJ_800.Helpers;

namespace T_MAJ_800.Repositories
{
    public class BlobRepository
    {
        private CloudBlobClient _blobClient;

        public BlobRepository()
        {
            if(CloudStorageAccount.TryParse(ConfigHelper.AZURE_STORAGE_CONNECTION_STRING, out CloudStorageAccount storageAccount))
            {
                _blobClient = storageAccount.CreateCloudBlobClient();
            }
            else
            {
                throw new System.Exception("Could not connect to the blob storage with the provided connection string");
            }
        }

        private CloudBlobContainer GetDatasetBlobContainer()
        {
            CloudBlobContainer container = _blobClient.GetContainerReference(ConfigHelper.DATASET_CONTAINER_NAME);
            container.CreateIfNotExists();
            return container;
        }

        private CloudBlockBlob GetDatasetBlob(CloudBlobContainer container, eDataset dataset)
        {
            return container.GetBlockBlobReference(dataset.ToString() + "/data.json");
        }

        public void UpdateDatasetBlobContent(eDataset dataset)
        {
            var container = GetDatasetBlobContainer();
            var blob = GetDatasetBlob(container, dataset);
            var data = HttpRepository.GetDataset(dataset);
            blob.UploadText(data);
        }

        public string GetDatasetFromBlob(eDataset dataset)
        {
            var container = GetDatasetBlobContainer();
            var blob = GetDatasetBlob(container, dataset);

            if(!blob.Exists())
            {
                UpdateDatasetBlobContent(dataset);
            }

            return blob.DownloadText();
        }


    }
}
