﻿using System.Linq;
using System.Text.RegularExpressions;

namespace T_MAJ_800.Helpers
{
    public static class TimeMetricsGenerator
    {
        private static readonly string monthlyPattern = @"\d{4}M\d{2}";
        private static readonly Regex monthlyRegex = new Regex(monthlyPattern);

        public static string CreateAbscissa(string lastValue, int offset)
        {
            // Year 
            if (int.TryParse(lastValue, out int output))
            {
                return (output + offset).ToString();
            }
            // Year + Month
            else if (monthlyRegex.IsMatch(lastValue))
            {
                int year = int.Parse(lastValue.Substring(0, 4));
                int month = int.Parse(lastValue.Substring(5, 2));

                if (month + offset > 12)
                {
                    year++;
                    month = (month + offset - 1) % 12 + 1;
                }
                else
                {
                    month += offset;
                }
                
                return $"{year}M{month:D2}";
            }
            return offset.ToString();
        }
    }
}
