﻿using T_MAJ_800.Enumerations;
using T_MAJ_800.Enumerations.Attributes;

namespace T_MAJ_800.Helpers.Enumeration
{
    public static class EnumDatasetUrlHelper
    {
        private static DatasetUrlAttribute GetAttributes(this eDataset enumVal)
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            if (memInfo == null || memInfo.Length == 0)
                return null;
            return (DatasetUrlAttribute)memInfo[0].GetCustomAttributes(typeof(DatasetUrlAttribute), false)[0];
        }

        public static string GetDatasetUrl(this eDataset enumVal)
        {
            var attributes = GetAttributes(enumVal);
            return attributes.Url;
        }

        public static string GetDatasetApiKey(this eDataset enumVal)
        {
            var attributes = GetAttributes(enumVal);
            return attributes.ApiKey;
        }
    }
}
