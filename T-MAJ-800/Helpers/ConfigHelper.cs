﻿using System;

namespace T_MAJ_800.Helpers
{
    public class ConfigHelper
    {
        public const string BASE_URL_STATEBANK_IRELAND = "https://statbank.cso.ie/StatbankServices/StatbankServices.svc/jsonservice/responseinstance/";

        public const string DATASET_CONTAINER_NAME = "datasets";

        public static string AZURE_STORAGE_CONNECTION_STRING {
            get {
                return Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");
            }
        }
    }
}
